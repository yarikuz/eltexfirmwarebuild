#!/bin/bash
BASE_DIR="$(dirname "$(readlink -f "$0")")"
FIRMWARE_DIR="$BASE_DIR/firmware"
SYSTEM_DIR="SYSTEM"
SYSTEM_APP_DIR="$SYSTEM_DIR/app"
SYSTEM_PRIV_APP_DIR="$SYSTEM_DIR/priv-app"
KEYS_DIR="$BASE_DIR/security"
BUILD_PROP="$SYSTEM_DIR/build.prop"
DEFAULT_KEY_NAME=testkey
CUSTOM_KEY_NAME=customkey
SYSTEM_OTACERTS_DIR="$SYSTEM_DIR/etc/security"
SYSTEM_OTACERTS_FILE="otacerts.zip"
FW_VERSION_PATH="$FIRMWARE_DIR/version"
JAR_TOOLS="$BASE_DIR/tools/framework"
ELT_NATIVE_LIB_EXTRACTOR="$BASE_DIR/tools/elt_native_lib_extractor"

ARCH=arm64-v8a
ARCH_TARGET=arm64
FALLBACK_ARCH=armeabi-v7a
ARCH_FALLBACK_TARGET=arm

## Setup PATH for MKBOOTIMG
export PATH=$PATH:$BASE_DIR/tools
export PATH=$PATH:$BASE_DIR/tools/lib
export MKBOOTIMG=mkbootimg

CHECK_UBUNTU() {
	OS_NAME=$(cat /etc/lsb-release | grep DISTRIB_ID | awk -F'=' '{print $2}')
	OS_VER=$(cat /etc/lsb-release | grep DISTRIB_RELEASE | awk -F'=' '{print $2}')

	echo "OS:" $OS_NAME $OS_VER

	OS_M_VER=$(echo "$OS_VER" | awk -F'.' '{print $1}')

	if [ "$OS_NAME" != "Ubuntu" ]; then echo "ERROR: Only Ubuntu OS supported"; exit -1; fi
	if [ "$OS_M_VER" -lt "16" ]; then echo "ERROR: Ubuntu version is too old"; exit -1; fi
}

CHECK_SYSTEM() {
	echo "### System environment ###"
	echo

	CHECK_UBUNTU

	## Get environment
	AVAIL_MEM_KBYTES=$(cat /proc/meminfo | grep MemAvailable | awk -F' ' '{print $2}')
	AVAIL_SWAP_KBYTES=$(cat /proc/meminfo | grep SwapFree | awk -F' ' '{print $2}')
	AVAIL_DISK_GBYTES=$(echo $(($(stat -f --format="%a*%S/(1024*1024*1024)" .))))

	VIRTUAL_BOX_USED=""
	if dmesg | grep DMI | grep VirtualBox | wc -l | grep -q '0'; then
		VIRTUAL_BOX_USED="false"
	else
		VIRTUAL_BOX_USED="true"
	fi

	CPU_CORES=$(cat /proc/cpuinfo | grep processor | wc -l)
	KERNEL_VER=$(uname -r | awk -F'-' '{print $1}')
	ZIP=$(which zip)
	JAVA=$(which java)
	PYTHON=$(which python)
	OPENSSL=$(which openssl)

	## Print environment
	echo "PATH:" $PATH
	echo "Free space on disk:" $AVAIL_DISK_GBYTES "Gb"
	echo "Available RAM:" $AVAIL_MEM_KBYTES "kb"
	echo "Available Swap:" $AVAIL_SWAP_KBYTES "kb"
	echo "VirtualBox using detected:" $VIRTUAL_BOX_USED
	echo "CPU cores:" $CPU_CORES
	echo "Kernel version:" $KERNEL_VER
	echo "Tool zip:" $ZIP
	echo "Tool java:" $JAVA
	echo "Tool python:" $PYTHON
	echo "Tool openssl:" $OPENSSL
	echo

	## Check errors
	KERNEL_M_VER=$(uname -r | awk -F'.' '{print $1}')
	if [ "$KERNEL_M_VER" -lt "3" ]; then echo "ERROR: Kernel is too old"; exit -1; fi
	if [ -z "$ZIP" ]; then echo "ERROR: zip is not installed (sudo apt install zip)"; exit -1; fi
	if [ -z "$JAVA" ]; then echo "ERROR: java is not installed (sudo apt install default-jre)"; exit -1; fi
	if [ -z "$PYTHON" ]; then echo "ERROR: python 2.7 is not installed (sudo apt install python)"; exit -1; fi
	if [ -z "$OPENSSL" ]; then echo "ERROR: openssl is not installed (sudo apt install openssl)"; exit -1; fi

	TOTAL_AVAIL_MEM_KBYTES=$(($AVAIL_MEM_KBYTES + $AVAIL_SWAP_KBYTES))
	if [ "$TOTAL_AVAIL_MEM_KBYTES" -lt "2000000" ]; then echo "ERROR: Too low memory available"; exit -1; fi

	if [ "$AVAIL_DISK_GBYTES" -lt "5" ]; then echo "ERROR: Too low space on disk available"; exit -1; fi
	echo
}

CHECK_SYSTEM

MODEL_TARGET="nv711" # Will be rewrited by collect_sdk.sh
HW_REV="revA" # This value is constant

## Read and check version name
FW_VERSION_FULL=$(<"$FW_VERSION_PATH")
echo "Version format checking..."
if [[ "$FW_VERSION_FULL" =~ ^[^-]*-([1-9][0-9]*|0)\.([1-9][0-9]*|0)\.([1-9][0-9]*|0)-b([1-9][0-9]*|0)$ ]]
then
    echo "Version format match."
else
    echo "Error! Firmare version doesn't match the pattern: package-X.Y.Z-bA"
    exit -1
fi

FW_PACKAGE="${FW_VERSION_FULL%%-*}"
FW_VERSION="${FW_VERSION_FULL#*-}"
OTA_NAME="fw${MODEL_TARGET##nv}-${HW_REV}-${FW_VERSION_FULL}.ota"

if [ -z "$1" ]; then
	MAIN_KEY="testkey"
else
	MAIN_KEY="$1"
fi

## Check if there are keys in SDK
if [[ ! -f "$KEYS_DIR"/"$MAIN_KEY".pk8 || ! -f "$KEYS_DIR"/"$MAIN_KEY".x509.pem ]]; then
	echo "Error! There is no one of the keys: $MAIN_KEY.pk8 or $MAIN_KEY.x509.pem"
	exit -2
fi

## Build LOGO
mkdir -p LOGO
logo_img_packer -r "$BASE_DIR/logo" LOGO/logo

extract_native() {
    local app="$1"
    local arch="$2"
    local app_dir="$3"
    app_is_archive=`file "$app" | grep "archive" | wc -l`
    if [[ "$app_is_archive" != 0 ]]
    then
        unzip -o "$app" "lib/$arch/*" -d "$app_dir" 2>/dev/null
    else
        "$ELT_NATIVE_LIB_EXTRACTOR" "$app" -a "$arch" -d "$app_dir" 2>/dev/null
    fi
}

prepare_apps() {
	echo "Prepare apps"
	local subdir="$1"
	local out_path="$SYSTEM_PRIV_APP_DIR"
	for app in "$BASE_DIR/$subdir"/*.apk
	do
		app_name=`basename "$app"`
		if [ ! -f "$BASE_DIR/$subdir/$app_name" ]
		then
			break
		fi
		app_dir=$out_path/`echo "$app_name" | sed 's/\.apk//'`
		echo "App name: $app_name"
		mkdir -p "$app_dir"
		rm -rf "$app_dir"/*
		cp "$app" "$app_dir"
		extract_native "$app" "$ARCH" "$app_dir"
		if [ -d "$app_dir/lib/$ARCH" ]
		then
			mv -T "$app_dir/lib/$ARCH" "$app_dir/lib/$ARCH_TARGET"
		fi

		extract_native "$app" "$FALLBACK_ARCH" "$app_dir"
		if [ -d "$app_dir/lib/$FALLBACK_ARCH" ]
		then
			mv -T "$app_dir/lib/$FALLBACK_ARCH" "$app_dir/lib/$ARCH_FALLBACK_TARGET"
		fi
	echo "done"
	done
}

clean_apps() {
	echo "Clean"
	local subdir="$1"
	local out_path="$SYSTEM_PRIV_APP_DIR"
	for app in "$BASE_DIR/$subdir"/*.apk
	do
		app_name=`basename "$app"`
		if [ ! -f "$BASE_DIR/$subdir/$app_name" ]
		then
			break
		fi
		echo "App name: $app_name"
		app_dir=$out_path/`echo "$app_name" | sed 's/\.apk//'`
		rm -rf "$app_dir"
	done
}

## Fix permissions just for case
# Apps on Android 7 are in separate directories
fix_permissions() {
	local DIR=$1
	local EXT=$2
	local PERMS=$3

	local dirpath=""

	for f in $(ls -R "$DIR")
	do
		local ext="${f#*.}"

		if [[ $ext = "$EXT" ]]; then
			chmod "$PERMS" "$dirpath/$f"
		elif [[ "$f" =~ .*\: ]]; then
			local dirpath="${f%%:}"
		fi
	done
}

## Copy custom apk in firmware
prepare_apps "apk"

## In case of customer forgot to change permissions for his new packages
fix_permissions "$SYSTEM_APP_DIR" "apk" 0664
fix_permissions "$SYSTEM_PRIV_APP_DIR" "apk" 0664

## Build images
mkdir -p IMAGES

## Update keys
pushd "$KEYS_DIR"
zip -q "$SYSTEM_OTACERTS_FILE" *.x509.pem
for KEY in ./*.x509.pem; do
	openssl x509 -pubkey -noout -in "$KEY" > "$KEYS_DIR"/$(basename -s .x509.pem "$KEY").pub.pem;
done
popd

## Copy new keys to system.img
cp -f "$KEYS_DIR"/"$SYSTEM_OTACERTS_FILE" "$SYSTEM_OTACERTS_DIR"/"$SYSTEM_OTACERTS_FILE"
rm -f "$SYSTEM_DIR"/etc/update_engine/*
cp -f "$KEYS_DIR"/*.pub.pem "$SYSTEM_DIR"/etc/update_engine

## Change version and build system.img
sed -i "s/ro.build.version.incremental=.*/ro.build.version.incremental=$HW_REV-$FW_VERSION_FULL/g" "$SYSTEM_DIR/build.prop"
./tools/releasetools/build_image.py "$SYSTEM_DIR" "$FIRMWARE_DIR/system_image_info.txt" IMAGES/system.img "$SYSTEM_DIR"

## Build bootable images (boot.img and recovery.img)
pushd IMAGES

# ---- boot.img ----
unzip -q "$FIRMWARE_DIR"/firmware.zip META/boot_filesystem_config.txt "BOOT/*"
sed -i "s/ro.build.version.incremental=.*/ro.build.version.incremental=$HW_REV-$FW_VERSION_FULL/g" BOOT/RAMDISK/default.prop
mkbootfs -f META/boot_filesystem_config.txt BOOT/RAMDISK | minigzip > BOOT/ramdisk.gz
$MKBOOTIMG --kernel BOOT/kernel --cmdline buildvariant=eng --base 0x0 --kernel_offset 0x1080000 --os_version 7.1.2 \
		--os_patch_level 2017-12-01 --ramdisk BOOT/ramdisk.gz -o "$BASE_DIR/IMAGES/boot.img"

# ---- recovery.img ----
unzip -q "$FIRMWARE_DIR/firmware.zip" META/recovery_filesystem_config.txt "RECOVERY/*"
sed -i "s/ro.build.version.incremental=.*/ro.build.version.incremental=$HW_REV-$FW_VERSION_FULL/g" RECOVERY/RAMDISK/default.prop
# Update all keys in recovery
java -jar "$JAR_TOOLS"/dumpkey.jar `ls "../security"/* | grep x509.pem` > RECOVERY/RAMDISK/res/keys
rm -f RECOVERY/RAMDISK/etc/update_engine/*
cp -f "$KEYS_DIR"/*.pub.pem RECOVERY/RAMDISK/etc/update_engine
mkbootfs -f META/recovery_filesystem_config.txt RECOVERY/RAMDISK | minigzip > RECOVERY/ramdisk.gz
$MKBOOTIMG --kernel RECOVERY/kernel --cmdline buildvariant=eng --base 0x0 --kernel_offset 0x1080000 --os_version 7.1.2 \
		--os_patch_level 2017-12-01 --ramdisk RECOVERY/ramdisk.gz -o "$BASE_DIR/IMAGES/recovery.img"

# And remove temporary data
rm -fr META/ BOOT/ RECOVERY/
rm -f "$KEYS_DIR"/"$SYSTEM_OTACERTS_FILE" "$KEYS_DIR"/*.pub.pem
popd

## Update targets
zip -qr "$FIRMWARE_DIR/firmware.zip" LOGO IMAGES SYSTEM
rm -fr IMAGES/ LOGO/

## Build OTA
./tools/releasetools/ota_from_target_files -v --block --dtb -k "$KEYS_DIR"/"$MAIN_KEY" -p "$BASE_DIR/tools" "$FIRMWARE_DIR/firmware.zip" "$OTA_NAME"
echo "Signed with $MAIN_KEY"
zip -qd "$FIRMWARE_DIR/firmware.zip" SYSTEM/\* IMAGES/system.img IMAGES/recovery.img IMAGES/boot.img

clean_apps "apk"

## Increment build number
FW_VERSION_XYZ="${FW_VERSION%-b*}"
FW_VERSION_BN="${FW_VERSION##$FW_VERSION_XYZ-b}"
let FW_VERSION_BN+=1
FW_VERSION="$FW_VERSION_XYZ-b$FW_VERSION_BN"
FW_VERSION_FULL="$FW_PACKAGE-$FW_VERSION"
echo "Save new build version: $FW_VERSION_FULL"
echo "$FW_VERSION_FULL" > "$FW_VERSION_PATH"
