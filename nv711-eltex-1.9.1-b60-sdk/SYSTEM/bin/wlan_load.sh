#!/system/bin/sh

TAG_NAME=`basename $0`
LOGD="log -p d -t $TAG_NAME"
LOGW="log -p w -t $TAG_NAME"
LOGE="log -p e -t $TAG_NAME"
LOGI="log -p i -t $TAG_NAME"

HAS_EMBEDDED_WIFI=`getprop ro.product.model | grep W | wc -l`

$LOGD "Board has embedded wifi: $HAS_EMBEDDED_WIFI"

WIFI_CHIP=""
WIFI_ID=""
WIFI_DRIVER=""
BT_CHIP=""
BT_ID=""
OLD_WIFI_ID=`getprop wifi.model.id`
SED="sed"

RTK_DEV_IDS=`lsusb | grep 'ID 0bda:' | cut -d ":" -f 3`
for dev_id in $RTK_DEV_IDS
do
    case "$dev_id" in
    "0811")
        WIFI_CHIP="RTL8811AU"
        WIFI_ID="0bda:0811"
        WIFI_DRIVER="8821au"
        ;;
    "0821")
        WIFI_CHIP="RTL8821AU"
        WIFI_ID="0bda:0821"
        WIFI_DRIVER="8821au"
        BT_CHIP="RTL8821AU"
        BT_ID="0bda:0821"
        ;;
    "0823")
        WIFI_CHIP="RTL8821AU"
        WIFI_ID="0bda:0823"
        WIFI_DRIVER="8821au"
        BT_CHIP="RTL8821AU"
        BT_ID="0bda:0823"
        ;;
    "c820")
        WIFI_CHIP="RTL8821CU"
        WIFI_ID="0bda:c820"
        WIFI_DRIVER="8821cu"
        BT_CHIP="RTL8821CU"
        BT_ID="0bda:c820"
        ;;
    "b82c")
        WIFI_CHIP="RTL8822BU"
        WIFI_ID="0bda:b82c"
        WIFI_DRIVER="8822bu"
        BT_CHIP="RTL8822BU"
        BT_ID="0bda:b82c"
        ;;
    "b761")
        BT_CHIP="RTL8761AU"
        BT_ID="0bda:b761"
        ;;
    esac
done

setprop "wifi.model.name" "$WIFI_CHIP"
setprop "wifi.model.id" "$WIFI_ID"
setprop "wifi.model.driver" "$WIFI_DRIVER"

if [ "$WIFI_CHIP" != "" ]
then
    $LOGD "WiFi model name = $WIFI_CHIP, model id = $WIFI_ID"
else
    $LOGD "We haven't WiFi!"
fi

setprop "bt.model.name" "$BT_CHIP"
setprop "bt.model.id" "$BT_ID"

if [ "$BT_CHIP" != "" ]
then
    $LOGD "Bluetooth model name = $BT_CHIP, model id = $BT_ID"
else
    $LOGD "We haven't Bluetooth!"
fi

if [ "$WIFI_DRIVER" != "" ] && [ "$WIFI_ID" != "$OLD_WIFI_ID" ]
then
    WLAN0_HWADDR=`cat /proc/cmdline | grep ' wmac=' | $SED 's/.*wmac=//' | cut -f 1 -d " "`
    if [ "$WLAN0_HWADDR" = "" ]; then
        # clone from eth0
        WLAN0_HWADDR=`cat /sys/class/net/eth0/address`
    fi
    $LOGD "Current wifi mac: $WLAN0_HWADDR"
    WLAN0_HWADDR_STRIPPED=`echo $WLAN0_HWADDR | tr -d ':' | tr '[:upper:]' '[:lower:]'`
    setprop "wifi.model.mac" "$WLAN0_HWADDR_STRIPPED"
fi

exit 0

