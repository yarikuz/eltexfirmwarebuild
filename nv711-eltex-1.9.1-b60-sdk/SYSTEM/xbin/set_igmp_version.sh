#!/system/bin/sh

TAG_NAME=`basename $0`
LOGD="log -p d -t $TAG_NAME"
LOGW="log -p w -t $TAG_NAME"
LOGE="log -p e -t $TAG_NAME"
LOGI="log -p i -t $TAG_NAME"

IGMP_VER_CONTROLLER="/proc/sys/net/ipv4/conf/all/force_igmp_version"
IGMP_VER_PROP="persist.vendor.eltex.igmp_ver"

$LOGD "Forcing IGMP version"

CUR_IGMP_VER=$(getprop $IGMP_VER_PROP)
if [[ "$CUR_IGMP_VER" == "" ]]
then
	$LOGD "Current IGMP version do not set"
	$LOGD "Default IGMP version: forced v2"
	CUR_IGMP_VER=2
	setprop $IGMP_VER_PROP $CUR_IGMP_VER
	$LOGD "Default IGMP version saved to system properties"
fi

case $CUR_IGMP_VER in
0)
	CUR_IGMP_VER_STR="Auto v2/v3"
	;;
1)
	CUR_IGMP_VER_STR="v1 only"
	;;
2)
	CUR_IGMP_VER_STR="v2 only"
	;;
*)
	$LOGW "Unknown IGMP version!"
	$LOGW "Skip forcing"
	exit
	;;
esac

$LOGD "Forced IGMP version: $CUR_IGMP_VER_STR"
echo $CUR_IGMP_VER > $IGMP_VER_CONTROLLER
$LOGD "Done"
