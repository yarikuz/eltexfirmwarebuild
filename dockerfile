FROM ubuntu:18.04
RUN apt-get update; \
    apt-get install -y vim; \
    apt-get install -y openssh-client; \
    apt-get install sudo; \
    apt-get install -y iputils-ping; \
    apt-get install -y gcc python; \
    apt-get install -y python-pip; \
    apt-get install -y zip; \
    apt-get install -y default-jre; \
    apt-get install bsdmainutils; \
    apt-get clean all
COPY . /tmp
RUN chmod 755 -R /tmp
WORKDIR /tmp
#RUN apt-get install -y /tmp/appstore_4.90-b32_all.deb
#RUN apt-get install -y -f
 
