#!/bin/bash

BASE_DIR="$(dirname "$(readlink -f "$0")")"
SDK_ROOT="$(readlink -f "$BASE_DIR/..")"

TOOLS_DIR="$SDK_ROOT/tools"
ELT_NATIVE_LIB_EXTRACTOR="$TOOLS_DIR/elt_native_lib_extractor"
IMGMKR="$TOOLS_DIR/imgmkr"
FW_VERSION_PATH="$SDK_ROOT/build/version"
FIRMWARE_DIR="$SDK_ROOT/firmware"
SYSTEM_DIR="$FIRMWARE_DIR/package5/system"
TMP_SYSTEM_DIR="$SDK_ROOT/.tmp"
BUILD_PROP="$SYSTEM_DIR/build.prop"
SYSTEM_APK_DIRECTORY="$SYSTEM_DIR/app"
SYSTEM_PRIV_APK_DIRECTORY="$SYSTEM_DIR/priv-app"
SYSTEM_LIB_DIRECTORY="$SYSTEM_DIR/lib"
SYSTEM_CACERTS_DIRECTORY="$SYSTEM_DIR/etc/security/cacerts"

INITIAL_PACKAGE="eltex"
INITIAL_VERSION="0.0.1-b0"
IS_PACKAGE_BUILTIN="0"
FW_VERSION_FULL="$INITIAL_PACKAGE-$INITIAL_VERSION"

ARCH=armeabi-v7a
FALLBACK_ARCH=armeabi

DEVICE_MODEL="$(cat .device_model)"
HW_REV="revA"

extract_native() {
    local app="$1"
    local arch="$2"
    local app_dir="$3"
    app_is_archive=`file "$app" | grep "archive" | wc -l`
    if [[ "$app_is_archive" != 0 ]]
    then
        unzip -o "$app" "lib/$arch/*" -d "$app_dir" 2>/dev/null
    else
        "$ELT_NATIVE_LIB_EXTRACTOR" "$app" -a "$arch" -d "$app_dir" 2>/dev/null
    fi
}

prepare_apps() {
    echo "Prepare apps"
    local subdir="$1"
    local app_dir="$SDK_ROOT/build/$subdir"
    local lib_dir="$app_dir/lib"
    for app in "$app_dir"/*.apk
    do
            app_name=`basename "$app"`
            if [ ! -f "$app_dir/$app_name" ]
            then
                    echo "File not found"
                    break
            fi
            echo "App name: $app_name"
            cp "$app" "$SYSTEM_PRIV_APK_DIRECTORY"
            extract_native "$app" "$ARCH" "$app_dir"
            if [ -d "$lib_dir/$ARCH" ]
            then
                    mv "$lib_dir/$ARCH"/* "$lib_dir"
                    rm -rf "$lib_dir/$ARCH"
            else
                    extract_native "$app" "$FALLBACK_ARCH" "$app_dir"
                    if [ -d "$lib_dir/$FALLBACK_ARCH" ]
                    then
                            mv "$lib_dir/$FALLBACK_ARCH"/* "$lib_dir"
                            rm -rf "$lib_dir/$FALLBACK_ARCH"
                    fi
            fi
    done
    if [ -d "$lib_dir" ]
    then
        cp "$lib_dir"/* "$SYSTEM_LIB_DIRECTORY"
    fi
    echo "done"
}

clean_apps() {
    local subdir="$1"
    local app_dir="$SDK_ROOT/build/$subdir"
    local lib_dir="$app_dir/lib"
    for app in "$app_dir"/*.apk
    do
        app_name=`basename "$app"`
        if [ ! -f "$app_dir/$app_name" ]
        then
            break
        fi
        echo "Clean app name: $app_name"
        rm -f "$SYSTEM_PRIV_APK_DIRECTORY/$app_name"
    done
    for lib in "$lib_dir"/*.so
    do
        lib_name=`basename "$lib"`
        if [ ! -f "$lib_dir/$lib_name" ]
        then
            break
        fi
        echo "Clean lib name: $lib_name"
        rm -f "$SYSTEM_LIB_DIRECTORY/$lib_name"
    done
    rm -rf "$lib_dir"
}

setCustomApkPermissions() {
    echo "Verifying apks permissions"
    chmod 0644 "$SYSTEM_APK_DIRECTORY"/* && chmod 0644 "$SYSTEM_PRIV_APK_DIRECTORY"/*
    if [[ $? != 0 ]]
    then
        echo "Something wrong with permissions. Err: $?"
        exit -1;
    else
        echo "Apks permissions verified"
    fi
}

setCustomCacertsPermissions() {
    echo "Verifying cacerts permissions"
    chmod 0664 "$SYSTEM_CACERTS_DIRECTORY"/*
    if [[ $? != 0 ]]
    then
        echo "Something wrong with permissions. Err: $?"
        exit -1;
    else
        echo "Cacerts permissions verified"
    fi
}

# Check version
if [[ ! -f "$FW_VERSION_PATH" ]]
then
    echo "Version file not found. Use default version."
    echo "$FW_VERSION_FULL" > "$FW_VERSION_PATH"
fi

echo "Version format checking..."
FW_VERSION_FULL=$(cat "$FW_VERSION_PATH")
if [[ "$FW_VERSION_FULL" =~ ^[^-]*-([1-9][0-9]*|0)\.([1-9][0-9]*|0)\.([1-9][0-9]*|0)-b([1-9][0-9]*|0)$ ]]
then
    echo "Version format match."
else
    echo "Error! Firmare version doesn't match the pattern: package-X.Y.Z-bA"
    exit -1
fi

FW_PACKAGE="${FW_VERSION_FULL%%-*}"
FW_VERSION="${FW_VERSION_FULL#*-}"

echo "Build FW with [$FW_VERSION_FULL] version"

prepare_apps "apk"
setCustomApkPermissions
setCustomCacertsPermissions

echo "Saving system dir..."
cp -r "$SYSTEM_DIR" "$TMP_SYSTEM_DIR"
echo "Saved"

sed -ri "s/^ro.build.version.incremental=.*$/ro.build.version.incremental=$HW_REV-$FW_PACKAGE-$FW_VERSION/" "$BUILD_PROP"

SYSTEM_TGZ=system.tar.gz
INSTALL_IMG=install.img

echo "Packing system..."
tar zcf "$FIRMWARE_DIR/package5/$SYSTEM_TGZ" -C "$SYSTEM_DIR" .
rm -rf "$SYSTEM_DIR" &>/dev/null
echo "System packed"

echo "Packing firmware..."
cd "$FIRMWARE_DIR"
tar cf "$SDK_ROOT/$INSTALL_IMG" -- *
cd -
echo "FW packed"

OUTPUT_FILE="fw$DEVICE_MODEL-$HW_REV-$FW_VERSION_FULL.fwe"

echo "Output file: $OUTPUT_FILE"
if [[ "$IS_PACKAGE_BUILTIN" != "0" ]]
then
    "$IMGMKR" --encrypt --output="$OUTPUT_FILE" --rev="$HW_REV" --version="$FW_VERSION" --model="nv$DEVICE_MODEL" "$SDK_ROOT/$INSTALL_IMG"
else
    "$IMGMKR" --encrypt --output="$OUTPUT_FILE" --rev="$HW_REV" --package="$FW_PACKAGE" --version="$FW_VERSION" --model="nv$DEVICE_MODEL" "$SDK_ROOT/$INSTALL_IMG"
fi
echo "Restoring system dir..."
mv "$TMP_SYSTEM_DIR" "$SYSTEM_DIR"
echo "Restored"

# Increment build number
FW_VERSION_XYZ="${FW_VERSION%-b*}"
FW_VERSION_BN="${FW_VERSION##$FW_VERSION_XYZ-b}"
let FW_VERSION_BN+=1
FW_VERSION="$FW_VERSION_XYZ-b$FW_VERSION_BN"
FW_VERSION_FULL="$FW_PACKAGE-$FW_VERSION"
echo "Save new build version: $FW_VERSION_FULL"
echo "$FW_VERSION_FULL" > "$FW_VERSION_PATH"

echo "Cleaning..."
rm "$FIRMWARE_DIR/package5/$SYSTEM_TGZ"
rm -rf "$TMP_SYSTEM_DIR" &>/dev/null
rm $SDK_ROOT/$INSTALL_IMG
clean_apps "apk"
echo "Cleaned"
